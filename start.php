<?php
/**
 * Languages plugin
 *
 */

elgg_register_event_handler('init', 'system', 'languages_init');

function languages_init() {

	register_translations(elgg_get_plugins_path() . "languages/languages/es");
	register_translations(elgg_get_plugins_path() . "languages/languages/nl");
	register_translations(elgg_get_plugins_path() . "languages/languages/de");
	register_translations(elgg_get_plugins_path() . "languages/languages/pt");
	register_translations(elgg_get_plugins_path() . "languages/languages/fr");
	register_translations(elgg_get_plugins_path() . "languages/languages/ca");
	register_translations(elgg_get_plugins_path() . "languages/languages/da");
	register_translations(elgg_get_plugins_path() . "languages/languages/eu");
	register_translations(elgg_get_plugins_path() . "languages/languages/gl");
	register_translations(elgg_get_plugins_path() . "languages/languages/it");
	register_translations(elgg_get_plugins_path() . "languages/languages/ja");
	register_translations(elgg_get_plugins_path() . "languages/languages/sr");
	register_translations(elgg_get_plugins_path() . "languages/languages/th");
	register_translations(elgg_get_plugins_path() . "languages/languages/zh");

	if (!elgg_is_logged_in()) {
		global $CONFIG;
		if ($useragent_language = languages_get_useragent_language()) {
			$CONFIG->language = $useragent_language;
		}
	}

}

function languages_get_useragent_language() {
	global $CONFIG;
	if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])) {

		register_translations($CONFIG->path . "languages/", true);

		$available_languages = array_keys($CONFIG->translations);
		$accepted_languages = explode(',', $_SERVER["HTTP_ACCEPT_LANGUAGE"]);

		foreach ($accepted_languages as $i => $accepted_language) {
			$accepted_languages[$i] = trim(array_shift(preg_split("/[-;]/", $accepted_language)));
		}

		$langs = array_intersect($accepted_languages, $available_languages);
		if (count($langs) > 0) {
			return array_shift($langs);
		}
		return false;
	}
}

